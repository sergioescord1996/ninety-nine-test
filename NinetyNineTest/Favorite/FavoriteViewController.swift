//
//  FavoriteViewController.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 3/8/21.
//

import UIKit

protocol FavoritePresenterLogic {
    func attach(_ view: FavoriteDisplayLogic)
    func retry()
}

class FavoriteViewController: UIViewController, FavoriteDisplayLogic {

    // MARK: - IBOutlets
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ricCodeLabel: UILabel!
    @IBOutlet weak var hotLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    // MARK: - Variables
    
    // MARK: - Constants
    private let presenter: FavoritePresenterLogic
    
    // MARK: - Initializers
    
    init(with presenter: FavoritePresenterLogic) {
        self.presenter = presenter
        super.init(nibName: Constants.Scenes.Favorite, bundle: Bundle.main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(self)
    }
    
    // MARK: - DisplayLogic
    
    func setupView(with model: FavoriteModel) {
        
        DispatchQueue.main.async { [weak self] in
            self?.loadingIndicator.stopAnimating()
            self?.loadingIndicator.isHidden = true
            self?.nameLabel.text = model.name
            self?.ricCodeLabel.text = model.ricCode
            self?.hotLabel.text = "\(model.hot)"
            self?.categoryLabel.text = model.category
        }
    }
    
    func showError(_ error: String) {
        let alertController = UIAlertController(title: "An unexpected error has occurred", message: error, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Retry", style: .cancel) { [weak self] _ in
            alertController.dismiss(animated: false) {
                self?.presenter.retry()
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { [weak self] _ in
            self?.dismiss(animated: true)
        }
        alertController.addAction(alertAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [weak self] in
            self?.present(alertController, animated: false)
        }
    }
    
    // MARK: - Private
    
}
