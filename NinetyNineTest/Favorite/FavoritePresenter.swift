//
//  FavoritePresenter.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 3/8/21.
//

import Foundation

protocol FavoriteDisplayLogic: AnyObject {
    func setupView(with model: FavoriteModel)
    func showError(_ error: String)
}

class FavoritePresenter: FavoritePresenterLogic {
    
    // MARK: - Variables
    
    weak var view: FavoriteDisplayLogic?
    var id: String?
    var model: FavoriteModel?
    var networkManager: NetworkManager?
    
    // MARK: - Presenter Logic
    
    func attach(_ view: FavoriteDisplayLogic) {
        self.view = view
        getFavorite()
    }
    
    func retry() {
        getFavorite()
    }
    
    // MARK: - Private
    
    private func getFavorite() {
        guard let id = self.id else { return }
        networkManager = NetworkManager(delegate: self)
        networkManager?.getFavorite(with: id)
    }
}

// MARK: - NetworkDelegate

extension FavoritePresenter: NetworkManagerDelegate {
    func didSuccess<T>(with data: T, _ networkManager: NetworkManager) {
        if let data = data as? Favorite {
            model = FavoriteModel(name: data.name, ricCode: data.ricCode, hot: data.hot, category: data.category)
            view?.setupView(with: model!)
        }
    }
    
    func didFail(with error: Error) {
        view?.showError(error.localizedDescription)
    }
}
