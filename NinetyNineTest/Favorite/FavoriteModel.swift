//
//  FavoriteModel.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 3/8/21.
//

import Foundation

struct FavoriteModel {
    let name: String
    let ricCode: String
    let hot: Int
    let category: String
}
