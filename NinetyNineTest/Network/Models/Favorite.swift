//
//  Favorite.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 2/8/21.
//

import Foundation

struct Favorite: Codable {
    let name: String
    let hot: Int
    let ricCode: String
    let category: String
}
