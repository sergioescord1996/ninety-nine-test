//
//  Favorites.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 2/8/21.
//

import Foundation

struct Favorites: Codable {
    let result: [String]
}
