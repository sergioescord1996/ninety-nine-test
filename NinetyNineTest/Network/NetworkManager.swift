//
//  NetworkManager.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 2/8/21.
//

import Foundation

protocol NetworkManagerDelegate: AnyObject {
    func didSuccess<T>(with data: T, _ networkManager: NetworkManager)
    func didFail(with error: Error)
}

struct NetworkManager {
    
    // MARK: - Delegate
    
    weak var delegate: NetworkManagerDelegate?
    
    // MARK: - Functions
    
    func getFavorites() {
        let urlString = "\(Constants.API.Base.serverUrl)\(Constants.API.Endpoints.favorites)"
        performRequest(with: urlString, where: Favorites.self)
    }
    
    func getFavorite(with id: String) {
        let urlString = "\(Constants.API.Base.serverUrl)\(Constants.API.Endpoints.favorites)\(id)"
        performRequest(with: urlString, where: Favorite.self)
    }
    
    // MARK: - Private
    
    private func performRequest<T>(with urlString: String, where type: T.Type) where T: Decodable {
        guard let url = URL(string: urlString) else { return }
        
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: url, cachedResponseOnError: true) { (data, response, error) in
            
            if let safeData = data {
                if let result: T = self.parseJSON(safeData) {
                    self.delegate?.didSuccess(with: result, self)
                }
            } else if let error = error {
                self.delegate?.didFail(with: error)
            }
        }
        
        task.resume()

    }
    
    private func parseJSON<T>(_ data: Data) -> T? where T: Decodable {
        let decoder = JSONDecoder()
        
        do {
            let data = try decoder.decode(T.self, from: data)
            
            return data
        } catch {
            delegate?.didFail(with: error)
            return nil
        }
    }
}
