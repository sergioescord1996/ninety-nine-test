//
//  UrlSession+Extensions.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 3/8/21.
//

import Foundation

extension URLSession {
    func dataTask(with url: URL,
                  cachedResponseOnError: Bool,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {

        return self.dataTask(with: url) { (data, response, error) in
            if cachedResponseOnError,
                let error = error,
                let cachedResponse = self.configuration.urlCache?.cachedResponse(for: URLRequest(url: url)) {
                
                completionHandler(cachedResponse.data, cachedResponse.response, error)
                return
            }

            completionHandler(data, response, error)
        }
    }
}
