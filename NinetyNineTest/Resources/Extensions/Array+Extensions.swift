//
//  Array+Extensions.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 2/8/21.
//

import Foundation

extension Array {
    func getElement(at index: Int) -> Element? {
        if 0..<self.count ~= index {
            return self[index]
        }
        
        return nil
    }
}
