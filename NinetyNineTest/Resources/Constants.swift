//
//  Constants.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 2/8/21.
//


struct Constants {
    struct Scenes {
        static let Main = "Home"
        static let Favorite = "Favorite"
    }
    
    struct API {
        struct Endpoints {
            static let favorites = "favorites/"
        }
        
        struct Base {
            static let serverUrl = "https://challenge.ninetynine.com/"
        }
    }
}
