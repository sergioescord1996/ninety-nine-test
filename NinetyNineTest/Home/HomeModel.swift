//
//  HomeModel.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 2/8/21.
//

import Foundation

struct HomeModel {
    
    var favorites: [Favorite]
    
    struct Favorite {
        let name: String
        let image: String = "flame"
    }
}
