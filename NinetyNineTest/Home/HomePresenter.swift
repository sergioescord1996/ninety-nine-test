//
//  HomeViewModel.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 2/8/21.
//

import Foundation

protocol HomeViewDisplayLogic: AnyObject {
    func setupView()
    func reloadView()
    func showError(_ error: String)
}

class HomePresenter: HomePresenterLogic {
    
    // MARK: - Variables
    
    weak var view: (HomeViewDisplayLogic & HomeRouter)?
    var model: HomeModel?
    var networkManager: NetworkManager?
    
    // MARK: - HomePresenterLogic
    
    func attach(_ view: HomeViewDisplayLogic & HomeRouter) {
        self.view = view
        view.setupView()
        getFavorites()
    }
    
    func getNumberOfRows() -> Int {
        return model?.favorites.count ?? 0
    }
    
    func getCellData(at index: Int) -> HomeModel.Favorite? {
        return model?.favorites.getElement(at: index)
    }
    
    func rowSelected(at index: Int) {
        guard let data = model?.favorites.getElement(at: index) else { return }
        view?.navigateToDetail(with: data.name)
    }
    
    func deleteRow(at index: Int) {
        model?.favorites.remove(at: index)
    }
    
    func retry() {
        self.getFavorites()
    }
    
    // MARK: - Private
    
    private func getFavorites() {
        networkManager = NetworkManager(delegate: self)
        networkManager!.getFavorites()
    }
}

// MARK: - NetworkManagerDelegate

extension HomePresenter: NetworkManagerDelegate {
    func didFail(with error: Error) {
        view?.showError(error.localizedDescription)
    }
    
    func didSuccess<T>(with data: T, _ networkManager: NetworkManager) {
        if let data = data as? Favorites {
            model = HomeModel(favorites: data.result.map({ HomeModel.Favorite(name: $0) }))
        }
        view?.reloadView()
    }
}
