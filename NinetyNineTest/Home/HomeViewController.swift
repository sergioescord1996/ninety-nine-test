//
//  HomeView.swift
//  NinetyNineTest
//
//  Created by Sergio Escalante Ordonez on 2/8/21.
//

import UIKit

protocol HomePresenterLogic {
    func attach(_ view: HomeViewDisplayLogic & HomeRouter)
    func getNumberOfRows() -> Int
    func getCellData(at index: Int) -> HomeModel.Favorite?
    func rowSelected(at index: Int)
    func deleteRow(at index: Int)
    func retry()
}

final class HomeViewController: UIViewController, HomeViewDisplayLogic {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var favoritesTableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    // MARK: - Variables
    
    
    // MARK: - Constants
    
    private let presenter: HomePresenterLogic?
    
    // MARK: - Initializers
    
    init(with presenter: HomePresenterLogic) {
        self.presenter = presenter
        super.init(nibName: Constants.Scenes.Main, bundle: Bundle.main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.attach(self)
    }
    
    // MARK: - HomeDisplayViewLogic
    
    func setupView() {
        self.title = "Stocks"
        favoritesTableView.delegate = self
        favoritesTableView.dataSource = self
        favoritesTableView.register(UITableViewCell.self, forCellReuseIdentifier: "FavoriteCell")
    }
    
    func reloadView() {
        DispatchQueue.main.async { [weak self] in
            self?.favoritesTableView.reloadData()
            self?.loadingIndicator.stopAnimating()
            self?.loadingIndicator.isHidden = true
        }
    }
    
    func showError(_ error: String) {
        let alertController = UIAlertController(title: "An unexpected error has occurred", message: error, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Retry", style: .cancel) { [weak self] _ in
            alertController.dismiss(animated: false) {
                self?.presenter?.retry()
            }
        }
        alertController.addAction(alertAction)
        
        DispatchQueue.main.async { [weak self] in
            self?.present(alertController, animated: false)
        }
    }
    
    // MARK: - Private
}

// MARK: - UITableViewDelegate

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter?.rowSelected(at: indexPath.row)
    }
}

// MARK: - UITableViewDatasource

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getNumberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteCell", for: indexPath)
        
        if let data = presenter?.getCellData(at: indexPath.row) {
            cell.textLabel?.text = data.name
            cell.imageView?.image = UIImage(systemName: data.image)
            cell.imageView?.tintColor = .systemOrange
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            presenter?.deleteRow(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}


// MARK: - Router Logic

protocol HomeRouter {
    func navigateToDetail(with id: String)
}

extension HomeViewController: HomeRouter {
    func navigateToDetail(with id: String) {
        let favoritePresenter = FavoritePresenter()
        favoritePresenter.id = id
        let favoriteViewController = FavoriteViewController(with: favoritePresenter)
        DispatchQueue.main.async { [weak self] in
            self?.present(favoriteViewController, animated: true)
        }
    }
}
